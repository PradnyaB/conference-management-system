var reviewerId = user.id;

fetch(`http://localhost:8080/getReviewPapers/${reviewerId}`)
  .then(response => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then(data => {
    let tableBody = document.getElementById('myTable');
    if (!tableBody) {
      console.error('Table body element not found');
      return;
    }

    data.forEach(obj => {
      let row = tableBody.insertRow();
      let paperIdCell = row.insertCell(0);
      let authorsCell = row.insertCell(1);
      let titleCell = row.insertCell(2);
      let pdfCell = row.insertCell(3);
      let reviewStatusCell = row.insertCell(4);
      paperIdCell.innerText = obj.publishPaperId;
      authorsCell.innerText = obj.authors.join(', ');
      titleCell.innerText = obj.title;
      reviewStatusCell.innerText = obj.reviewStatus;
      if (obj.reviewStatus == "NotReviewed") { reviewStatusCell.style.color = 'blue'; }
      else if (obj.reviewStatus == "Accepted") { reviewStatusCell.style.color = 'green'; }
      else { reviewStatusCell.style.color = 'red'; }
      pdfCell.innerHTML = `<a href="http://localhost:8080/getPdf/${obj.publishPaperId}">PDF Link</a>`;
    });
  })
  .catch(error => {
    console.error('Error:', error);
  });


let reviewstatus = document.getElementById("reviewstatus");
reviewstatus.onclick = ()=> {
    window.location.href = "/updatestatus"
}