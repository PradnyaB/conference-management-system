let conferenceId = document.getElementById("span").innerText;
sessionStorage.setItem('conferenceId', conferenceId);
fetch(`http://localhost:8080/getSubmission/${conferenceId}`)
  .then(response => response.json())
  .then(data => {
    const table = document.getElementById('myTable');
    let tableBody = document.getElementById('tableBody');
    if (data.length === 0) {
                table.style.display = 'none';
    }
    data.forEach(obj => {
      let row = tableBody.insertRow();
      let paperIdCell = row.insertCell(0);
      let authorsCell = row.insertCell(1);
      let titleCell = row.insertCell(2);
      let reviewerCell = row.insertCell(3);
      let pdfCell = row.insertCell(4);
      paperIdCell.innerText = obj.publishPaperId;
      authorsCell.innerText = obj.authors.join(', ');
      titleCell.innerText = obj.title;
      if(obj.reviewer == null) { reviewerCell.innerText = "Not Assigned !"; }
      else { reviewerCell.innerText = obj.reviewer.email; }
      pdfCell.innerHTML = `<a href="http://localhost:8080/getPdf/${obj.publishPaperId}">PDF Link</a>`;
    });
  })
  .catch(error => {
    console.error('Error:', error);
  });

let addReviewer = document.getElementById("addReviewer");

addReviewer.onclick = ()=> {
    window.location.href = "http://localhost:8080/addreviewer";
}

let status = document.getElementById("status");

status.onclick = ()=> {
    window.location.href = "http://localhost:8080/reviewstatus";
}

