var registrationForm = document.getElementById('registration-form');

if (registrationForm) {
    registrationForm.addEventListener('submit', function (event) {
        event.preventDefault();
        console.log("here");

        const conferenceTitle = document.getElementById('conferencetitle').value;
        const acronym = document.getElementById('acronym').value;
        const venue = document.getElementById('venue').value;
        const city = document.getElementById('city').value;
        const country = document.getElementById('country').value;
        const noOfSubmission = document.getElementById('no-of-sub').value;
        const firstDate = document.getElementById('Fdate').value;
        const lastDate = document.getElementById('Ldate').value;
        const primaryArea = document.getElementById('parea').value;
        const secondaryArea = document.getElementById('sarea').value;
        const organizer = document.getElementById('organizer').value;
        const contactNo = document.getElementById('phone-number').value;

        console.log(acronym, venue, city, country, noOfSubmission, firstDate, lastDate, primaryArea, secondaryArea, organizer, contactNo);
        var user = JSON.parse(sessionStorage.getItem('user'));
        var customerId = user.id;
        const apiUrl = `http://localhost:8080/addConference/${customerId}`// Replace with your API endpoint URL

        // Making a POST request to the API endpoint using Fetch API
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "conferenceTitle": conferenceTitle,
                "acronym": acronym,
                "venue": venue,
                "city": city,
                "country": country,
                "noOfSubmission": noOfSubmission,
                "primaryArea": primaryArea,
                "secondaryArea": secondaryArea,
                "organizer": organizer,
                "contactNo": contactNo,
                "firstDate": firstDate,
                "lastDate": lastDate
            })
        })
        .then(response => {
            console.log("success");
            window.location.href = '/myconference';
        })
        .catch(error => {
            console.error("Fetch Error:", error);
        });
    });
}
