 fetch('http://localhost:8080/getAllConference')
            .then(response => response.json())
            .then(data => {
                const tableBody = document.getElementById('conference-table-body');
                const currentDate = new Date();
                console.log(tableBody)
                data.forEach(conference => {
                    const firstDate = new Date(conference.firstDate);
                    const lastDate = new Date(conference.lastDate);

                    if (currentDate >= firstDate && currentDate <= lastDate) {
                        console.log("hello moto")
                        const row = document.createElement('tr');
                        row.innerHTML = `
                            <td>${conference.conferenceId}</td>
                            <td>${conference.conferenceTitle}</td>
                            <td><a href="/preprintform"> ${conference.acronym} </a></td>
                            <td>${conference.firstDate}</td>
                            <td>${conference.lastDate}</td>
                        `;
                        tableBody.appendChild(row);
                    }
                });
            })
            .catch(error => {
                console.error('Error fetching conference data:', error);
            });