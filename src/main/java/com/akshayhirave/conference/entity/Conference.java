package com.akshayhirave.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "conference")
public class Conference {

    @Id
    @SequenceGenerator(
            name = "conference_id",
            sequenceName = "conference_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "conference_sequence"
    )
    private Long conferenceId;

    @Column(
            name = "conference_title"
    )
    private String conferenceTitle;

    @Column(
            name = "acronym"
    )
    private String acronym;

    @Column(
            name = "venue"
    )
    private String venue;

    @Column(
            name = "city"
    )
    private String city;

    @Column(
            name = "country"
    )
    private String country;

    @Column(
            name = "no_of_submission"
    )
    private Long noOfSubmission;

    @Column(
            name = "first_date"
    )
    private LocalDate firstDate;

    @Column(
            name = "last_date"
    )
    private LocalDate lastDate;

    @Column(
            name = "primary_area"
    )
    private String primaryArea;

    @Column(
            name = "secondary_area"
    )
    private String secondaryArea;

    @Column(
            name = "organizer"
    )
    private String organizer;

    @Column(
            name = "contact_no"
    )
    private Long contactNo;

    @ManyToOne
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "conference_customer_fk"
            )
    )
    private CustomerSignUp customerSignUp;

    public Conference(String conferenceTitle, String acronym, String venue, String city, String country, Long noOfSubmission, LocalDateTime firstDate, LocalDateTime lastDate, String primaryArea, String secondaryArea, String organizer, Long contactNo) {
        this.conferenceTitle = conferenceTitle;
        this.acronym = acronym;
        this.venue = venue;
        this.city = city;
        this.country = country;
        this.noOfSubmission = noOfSubmission;
        this.firstDate = LocalDate.from(firstDate);
        this.lastDate = LocalDate.from(lastDate);
        this.primaryArea = primaryArea;
        this.secondaryArea = secondaryArea;
        this.organizer = organizer;
        this.contactNo = contactNo;
    }
}
