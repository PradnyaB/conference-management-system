package com.akshayhirave.conference.entity;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "signup")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerSignUp {

    @Id
    @SequenceGenerator(
            name = "user_sequence",
            sequenceName = "user_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_generator"
    )
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    public CustomerSignUp(String firstName,String lastName,String email, String password) {
        this.firstName= firstName;
        this.lastName= lastName;
        this.email = email;
        this.password = password;
    }
}
