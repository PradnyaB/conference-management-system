package com.akshayhirave.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class Customer {

    private final String username;
    private final String password;
}
