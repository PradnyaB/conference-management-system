package com.akshayhirave.conference.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "publish_paper")
public class PublishPaper {

    @Id
    @SequenceGenerator(
            name = "publish_paper_id",
            sequenceName = "publish_paper_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "publish_paper_sequence"
    )
    private Long publishPaperId;

    @Column(
            name = "authors"
    )
    private String[] authors;

    @Column(
            name = "title"
    )
    private String title;

    @Column(
            name= "abstract"
    )
    private String abstractString;

    @Column(
            name = "keywords"
    )
    private String keywords;

    @Column(
            name = "date"
    )
    private LocalDateTime date;

    @Lob
    @Column(
            name = "paper"
    )
    private byte[] paper;

    @ManyToOne
    @JoinColumn(
            name = "conference_id",
            referencedColumnName = "conferenceId",
            foreignKey = @ForeignKey(
                    name = "publish_paper_conference_fk"
            )
    )
    private Conference conference;

    @ManyToOne
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "publish_paper_customer_fk"
            )
    )
    private CustomerSignUp author;

    @ManyToOne
    @JoinColumn(
            name = "reviewer_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "publish_paper_reviewer_fk"
            )
    )
    private CustomerSignUp reviewer;

    public enum ReviewStatus {
        Accepted,
        Rejected,
        NotReviewed
    }
    private ReviewStatus reviewStatus = ReviewStatus.NotReviewed;

    private String feedback = "No Feedback send !";
}
