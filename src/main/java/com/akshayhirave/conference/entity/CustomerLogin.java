package com.akshayhirave.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder
public class CustomerLogin {

    private String email;
    private String password;
}
