package com.akshayhirave.conference.repository;

import com.akshayhirave.conference.entity.Conference;
import com.akshayhirave.conference.entity.PublishPaper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> {
    @Query(value = "select c from Conference c where c.customerSignUp.id = ?1")
    List<Conference> getConference(Long customerId);

    @Query("SELECT c FROM Conference c")
    List<Conference> getAllConferences();

}
