package com.akshayhirave.conference.repository;

import com.akshayhirave.conference.entity.CustomerSignUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepo extends JpaRepository<CustomerSignUp,Long> {

    @Query(value = "select c from CustomerSignUp c where c.email = ?1")
    CustomerSignUp findByEmail(String email);
}
