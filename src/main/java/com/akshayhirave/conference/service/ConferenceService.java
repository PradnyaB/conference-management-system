package com.akshayhirave.conference.service;

import com.akshayhirave.conference.entity.Conference;
import com.akshayhirave.conference.entity.CustomerSignUp;
import com.akshayhirave.conference.entity.PublishPaper;
import com.akshayhirave.conference.repository.ConferenceRepository;
import com.akshayhirave.conference.repository.CustomerRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
@AllArgsConstructor
public class ConferenceService {

    private final CustomerRepo customerRepo;

    private final ConferenceRepository conferenceRepository;

    public ResponseEntity<List<Conference>> getConference(Long customerId) {

        return ResponseEntity.ok(conferenceRepository.getConference(customerId));
    }
    public String addConference(Conference conference, Long customerId) {

        CustomerSignUp customerSignUp = customerRepo.findById(customerId).get();
        conference.setCustomerSignUp(customerSignUp);
        conferenceRepository.save(conference);
        return "conference added";
    }

    public ResponseEntity<List<Conference>> getAllConference() {
        return ResponseEntity.ok(conferenceRepository.getAllConferences());

    }
}
