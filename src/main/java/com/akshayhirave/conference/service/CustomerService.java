package com.akshayhirave.conference.service;

import com.akshayhirave.conference.entity.Customer;
import com.akshayhirave.conference.entity.CustomerLogin;
import com.akshayhirave.conference.repository.CustomerRepo;
import com.akshayhirave.conference.entity.CustomerSignUp;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepo customerRepo;
    public CustomerSignUp signUpCustomer(CustomerSignUp customerSignUp) {

        if(customerRepo.findByEmail(customerSignUp.getEmail()) != null) {
            return null;
        }
        customerRepo.save(customerSignUp);
        return customerSignUp;
    }

    public CustomerSignUp loginCustomer(CustomerLogin customerLogin) {

        if(customerRepo.findByEmail(customerLogin.getEmail()) == null) {
            return null;
        }

        CustomerSignUp customer = customerRepo.findByEmail(customerLogin.getEmail());
        if(customer.getPassword().equals(customerLogin.getPassword())) {
            return customer;
        } else {
            return null;
        }
    }

    public CustomerSignUp getCustomer(String customerEmail) {
        if(customerRepo.findByEmail(customerEmail) == null) {
            return null;
        }
        return  customerRepo.findByEmail(customerEmail);


    }
}
