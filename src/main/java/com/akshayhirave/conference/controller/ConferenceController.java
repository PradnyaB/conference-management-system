package com.akshayhirave.conference.controller;

import com.akshayhirave.conference.entity.Conference;
import com.akshayhirave.conference.entity.PublishPaper;
import com.akshayhirave.conference.service.ConferenceService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ConferenceController {

    private final ConferenceService conferenceService;

    @PostMapping("/addConference/{customerId}")
    private String addConference(@RequestBody Conference conference, @PathVariable(name = "customerId") Long customerId) {
        return conferenceService.addConference(conference, customerId);
    }
    @GetMapping("/getConference/{customerId}")
    private ResponseEntity<List<Conference>> getConference(@PathVariable(name = "customerId") Long customerId) {
        return conferenceService.getConference(customerId);
    }
    @GetMapping("/getAllConference")
    private ResponseEntity<List<Conference>> getAllConference() {
        return conferenceService.getAllConference();
    }
}
